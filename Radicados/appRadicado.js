  /*validador */
  $(document).ready(function () {

    validador = $("#formAdd").validate({
        rules: {
            buscarDocumento: {
                required: true,
                number:true,
                max: 14
    
            }
    
        },
    
        messages: {
            buscarDocumento: {
                required: "Numero de Radicado : Requerida",
                number:"Solo se resiven Numeros",
                max: "Ingresa un valor menor o igual que 14"
                
            },
           
    
        },
        errorPlacement: function (error, element) {
            $(element)
                .closest("form")
                .find("div[for='" + element.attr("id") + "']")
                .append(error);
        },
        errorElement: "span",
        onfocusout: function (element) {
            this.element(element);
        }
    
    
        
    });
    
    $("#fechaRadicado").prop("disabled", true);
    $("#nombreUsuario").prop("disabled", true);
    $("#loginUsuario").prop("disabled", true);
    $("#codigoUsuarioPendencia").prop("disabled", true);
    $("#codigoUsuario").prop("disabled", true);
    $("#estadoUsuario").prop("disabled", true);
    $("#CorreoUsuario").prop("disabled", true);
    $("#btnEnviar").prop("disabled", true);
    


  });

