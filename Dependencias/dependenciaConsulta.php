<?php

//SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

///INCLUSION DE LA BASE DE DATOS Y CREACION DEL OBJETO DE CONEXION

include('../conexion/conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();


    
    //HACER CONSULTA SQL
    //SI OBTENGO EL ID DE ESTUDIANTE MUESTRO TODA LA INFORMACION
    $sql ='SELECT * FROM public."USUARIO"';
    $stmt = $conexion->prepare($sql);
    $stmt->execute();
    
    
    
    // VERIFICO SI HAY INFORMACION EN LA BASE DE DATOS
    if($stmt->rowCount() > 0){
        // CREACION DEL ARREGLO ESTUDIANTE
        $usuario_array = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $post_data = [
                'USUA_NOMB' => $row['USUA_NOMB'],
                'USUA_LOGIN' => $row['USUA_LOGIN'],
                'DEPE_CODI' => $row['DEPE_CODI'],
                'USUA_ESTA' => $row['USUA_ESTA'],
               
            ];
            // ASIGNO LOS VALORES EN EL ARREGLO
            array_push($usuario_array, $post_data);
        }
        // MUESTRO LA INFORMACION EN FORMATO JSON
        echo json_encode($usuario_array);
    }
    else{
        // SI NO HAY INFORMACION EN LA BASE DE DATOS
        echo json_encode(['ESTADO'=>'ERROR']);
    }
    
   



?>
