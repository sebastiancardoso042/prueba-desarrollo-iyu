
$(document).ready(function () {


    /*validador */
    validador = $("#formAdd").validate({
        rules: {
            buscarDocumento: {
                required: true,

            },CorreoUsuario: {
				required: true,
				email: true
			},

        },

        messages: {
            buscarDocumento: {
                required: "Documento Usuario: Requerida",
            },
            CorreoUsuario: {
				required: 'Falta rellenar un correo electr\u00f3nico.',
				email: 'Falta rellenar el correo  con el formato correcto.'
			},

        },
        errorPlacement: function (error, element) {
            $(element)
                .closest("form")
                .find("div[for='" + element.attr("id") + "']")
                .append(error);
        },
        errorElement: "span",
        onfocusout: function (element) {
            this.element(element);
        }
    });


    $("#nombreUsuario").prop("disabled", true);
    $("#loginUsuario").prop("disabled", true);
    $("#codigoUsuarioPendencia").prop("disabled", true);
    $("#codigoUsuario").prop("disabled", true);
    $("#estadoUsuario").prop("disabled", true);
    $("#CorreoUsuario").prop("disabled", true);
    $("#btnEnviar").prop("disabled", true);
    
   
    $("#btnConsultar").click(function () {
        if (validador.form()) {
            var url = "usuarioConsulta.php";
            $.ajax({
                type: "POST",
                url: 'usuarioConsulta.php',
                data: {
                    documento: $("#buscarDocumento").val()
                },
                success: function (data) {
                    console.log(data);
                    if (data.ESTADO == "ERROR") {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'La persona no está registrada',
                        })
                        console.log('si sirve');

                    } else {

                        Swal.fire({
                            icon: 'success',
                            title: 'Exito',
                            text: 'La persona si está registrada',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $('#nombreUsuario').val(data[0]['USUA_NOMB']);
                        $('#loginUsuario').val(data[0]['USUA_LOGIN']);
                        $('#codigoUsuarioPendencia').val(data[0]['DEPE_CODI']);
                        $('#codigoUsuario').val(data[0]['USUA_DOC']);
                        $('#estadoUsuario').val(data[0]['USUA_ESTA']);
                        $('#CorreoUsuario').val(data[0]['USUA_EMAIL']);
                        $("#CorreoUsuario").prop("disabled", false);
                        $("#btnEnviar").prop("disabled", false);

                    }

                }
            });

        }


    });

    $("#btnEnviar").click(function () {
        if (validador.form()) {
            
            $.ajax({
                type: "POST",
                url: 'editarCorreo.php',
                data: {
                    documento: $("#buscarDocumento").val(),
                    editarCorreo: $("#CorreoUsuario").val()
                },
                success: function (data) {
                   
                    if (data.ESTADO == "ERROR") {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'No se pudo editar el correo',
                        })
                    }else{
                        Swal.fire({
                            icon: 'success',
                            title: 'Exito',
                            text: 'Correo editado ',
                            showConfirmButton: false,
                            timer: 1500
                        })

                        window.location.reload();

                    }

                    if(data.ESTADO == "CORREONO"){
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Pofavor digite un correo ',
                            
                          })
                    }


                }
            });

        }


    });


});



