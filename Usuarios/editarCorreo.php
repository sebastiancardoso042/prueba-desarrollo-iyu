<?php
//SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=UTF-8");

///INCLUSION DE LA BASE DE DATOS Y CREACION DEL OBJETO DE CONEXION

include('../conexion/conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();



// VERIFICACION DEL RECIBIMIENTO DE INFORMACION
if(isset($_POST['documento']) && isset($_POST['editarCorreo'])){
    
    $post_doc = $_POST['documento'];
    $post_correo = $_POST['editarCorreo'];

       
        $insert_query = 'UPDATE "USUARIO" SET "USUA_EMAIL"=? WHERE "USUA_DOC"=?';
        $insert_stmt = $conexion->prepare($insert_query);
        $insert_stmt->execute(array($post_correo,$post_doc));
        // DATA BINDING

        if($insert_stmt->execute()){
            $msg['message'] = 'Datos Insertados Correctamente';
        }else{
            $msg['ESTADO'=>'ERROR'];
        } 
        
    
}
else{
    $msg['ESTADO'=>'CORREONO'];
}
// SALIDA EN FORMATO JSON
echo  json_encode($msg);
